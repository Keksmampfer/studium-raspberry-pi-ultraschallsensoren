#!/usr/bin/python2.7


__author__ = 'Johannes'
from collections import deque
import time

from SRF10 import usensor
from Servo import servo
from circularbuffer import CircularBuffer

class mechaniccontrol():
    def __init__(self, mech):
        self.__mechanic__ = mech
        self.__lastAngle__ = 90
        self.__mechanic__.setAngle(self.__lastAngle__)

    def setAngle(self, grad):
        if grad != self.__lastAngle__:
            self.__mechanic__.setAngle(grad)
            self.__lastAngle__ = grad


if __name__ == '__main__':
    sensor1 = usensor.SRF10(0x70)
    sensor2 = usensor.SRF10(0x71)
    mechanic = mechaniccontrol(servo.mechanic(18))
    sensor1.setGain(8)
    sensor1.setRange(24)

    sensor2.setGain(8)
    sensor2.setRange(24)

    leftbuffer = CircularBuffer(30)
    rightbuffer = CircularBuffer(30)

    for i in range(30):
        leftbuffer.append(0)
        rightbuffer.append(0)

    while True:
        sensor1.startScan()

        while sensor1.onScan():
            pass
        sensor1erg = sensor1.getResult()

        if sensor1erg != 0:
            leftbuffer.popleft()
            leftbuffer.append(sensor1erg)

        time.sleep(0.01)
        sensor2.startScan()

        while sensor2.onScan():
            pass

        sensor2erg = sensor2.getResult()

        if sensor2erg != 0:
            rightbuffer.popleft()
            rightbuffer.append(sensor2erg)

        time.sleep(0.01)
        #Centercheck
        differenz = leftbuffer.median - rightbuffer.median

        if 10 > differenz > -10:
            mechanic.setAngle(90)
            print "Mitte, Left.av:", leftbuffer.median, " right.av:", rightbuffer.median
        elif 10 < differenz:
            mechanic.setAngle(135)
            print "Links, Left.av:", leftbuffer.median, " right.av:", rightbuffer.median
        else:
            mechanic.setAngle(45)
            print "Rechts, Left.av:", leftbuffer.median, " right.av:", rightbuffer.median





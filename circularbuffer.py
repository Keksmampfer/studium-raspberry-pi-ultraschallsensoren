__author__ = 'Johannes'
from collections import deque

class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)

    @property
    def average(self):
        return sum(self) / len(self)

    @property
    def median(self):
        l = []
        for i in self:
            l.append(i)
        l.sort()
        return l[len(l) / 2]
__author__ = 'Johannes'

from RPIO import PWM


class mechanic:
    __left__ = 550
    __right__ = 2380

    __center__ = __right__ - __left__ / 2


    def __init__(self, gpio):
        self.gpio = gpio
        self.servo = PWM.Servo()
        self._setServo(self.__center__)


    def setAngle(self, angle):
        if angle >= 0 and angle <= 180:
            pos = self.__left__ + ((self.__right__ - self.__left__) * angle) / 180
            self._setServo(pos)

    def _setServo(self, wert):
        self.servo.set_servo(self.gpio, int(10 * round(float(wert) / 10)))
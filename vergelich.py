#!/usr/bin/python2.7

__author__ = 'Johannes'
import time
from SRF10 import usensor
from PC74HCT50P import ioexpander
if __name__ == '__main__':
    display = ioexpander.PC74HCT50P(0x20)
    sensor1 = usensor.SRF10(0x70)
    sensor2 = usensor.SRF10(0x71)

    while True:


        sensor1.setGain(8)
        sensor1.setRange(24)

        sensor2.setGain(8)
        sensor2.setRange(24)

        sensor1.startScan()
        sensor2.startScan()

        # wait for finish both scans
        while sensor1.onScan() or sensor2.onScan():
            pass

        result1 = sensor1.getResult()
        result2 = sensor2.getResult()
        print('1:', result1)
        print('2:', result2)
        if( result1 == result2):
            print ('Beide gleich')
            display.setPortActive(0xff)

        if(result1 < result2):
            print('Links')
            display.setPortActive(0xf0)
            display.setPortInactive(0x0f)

        if(result2 < result1):
            print ('Rechts')
            display.setPortActive(0x0f)
            display.setPortInactive(0xf0)

        time.sleep(0.5)
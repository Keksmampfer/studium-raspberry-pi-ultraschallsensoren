#!/usr/bin/python2.7
import smbus

bus = smbus.SMBus(1)
address = 0x20


class Port(object):
    P0 = 0x01
    P1 = 0x02
    P2 = 0x04
    P3 = 0x08
    P4 = 0x10
    P5 = 0x20
    P6 = 0x40
    P7 = 0x80


def activate(ports):
    bus.read_byte_data(address, ports)


def read(ports):
    read = bus.read_byte(address)
    return read & ports


if __name__ == "__main__":
    activate(Port.P1 | Port.P5)
    buff = read(Port.P1 | Port.P5)
    if (buff & Port.P1) == Port.P1:
        print "Port P1 ist an"
    else:
        print "Port P1 ist aus"

__author__ = 'Johannes'
import smbus


class Port(object):
    P0 = 0x01
    P1 = 0x02
    P2 = 0x04
    P3 = 0x08
    P4 = 0x10
    P5 = 0x20
    P6 = 0x40
    P7 = 0x80


class PC74HCT50P():
    bus = smbus.SMBus(1)

    def __init__(self, address):
        self.__address__ = address
        self.__activePorts__ = 0x00
        self.__setIt()

    def setPortActive(self, ports):
        self.__activePorts__ = self.__activePorts__ | ports
        self.__setIt()

    def __setIt(self):
        self.bus.write_byte(self.__address__, self.__activePorts__)


    def setPortInactive(self, ports):
        self.__activePorts__ = (self.__activePorts__ & ports) ^ self.__activePorts__
        self.__setIt()


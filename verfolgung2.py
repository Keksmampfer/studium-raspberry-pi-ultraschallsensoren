#!/usr/bin/python2.7


__author__ = 'Johannes'
from collections import deque
from SRF10 import usensor
from PC74HCT50P import ioexpander
from Servo import servo


class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)

    @property
    def average(self):
        return sum(self) / len(self)


if __name__ == '__main__':
    display = ioexpander.PC74HCT50P(0x20)
    sensor1 = usensor.SRF10(0x70)
    sensor2 = usensor.SRF10(0x71)
    mechanic = servo.mechanic(18)
    sensor1.setGain(8)
    sensor1.setRange(24)

    sensor2.setGain(8)
    sensor2.setRange(24)

    leftbuffer = CircularBuffer(30)
    rightbuffer = CircularBuffer(30)

    for i in range(30):
        leftbuffer.append(0)
        rightbuffer.append(0)

    while True:
        sensor1.startScan()

        while sensor1.onScan():
            pass
        sensor1erg = sensor1.getResult()

        if sensor1erg != 0:
            leftbuffer.popleft()
            leftbuffer.append(sensor1erg)

        sensor2.startScan()

        while sensor2.onScan():
            pass

        sensor2erg = sensor2.getResult()

        if sensor2erg != 0:
            rightbuffer.popleft()
            rightbuffer.append(sensor2erg)


#!/usr/bin/python2.7
import time

import smbus


bus = smbus.SMBus(1)
sensorAddress = 0x70
displayAddress = 0x20


class Port(object):
    P0 = 0x01
    P1 = 0x02
    P2 = 0x04
    P3 = 0x08
    P4 = 0x10
    P5 = 0x20
    P6 = 0x40
    P7 = 0x80


if __name__ == "__main__":

    while True:
        #starte messvorgang
        bus.write_i2c_block_data(sensorAddress, 0x00, [0x51])
        time.sleep(0.1)
        while bus.read_i2c_block_data(sensorAddress, 0x00) == 255:
            time.sleep(0.1)
        buf = bus.read_byte_data(sensorAddress, 0x03)
    bus.read_byte_data(displayAddress, buf)
#!/usr/bin/python2.7
from SRF10 import usensor

__author__ = 'Johannes'

if __name__ == '__main__':
    sensor1 = usensor.SRF10(0x71)

    # 2m Scanrange
    sensor1.setGain(8)
    sensor1.setRange(45)

    sensor1.startScan()
    while sensor1.onScan():
        pass

    print(sensor1.getResult())


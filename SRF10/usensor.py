__author__ = 'Johannes'
import smbus


class ScanType():
    Zoll = 0x50
    Centimeter = 0x51
    Microseconds = 0x52


class SRF10():
    class Register():
        first = 0
        second = 1
        third = 2
        fourth = 3

    bus = smbus.SMBus(1)

    def __init__(self, address):
        self.__range__ = 0xff
        self.__address__ = address
        self.__gain__ = 0x10
        self.__scantype__ = ScanType.Centimeter

    def setRange(self, distance):
        self.__range__ = distance

    def setGain(self, gain):
        self.__gain__ = gain

    def startScan(self):
        self.bus.write_i2c_block_data(self.__address__, self.Register.third, [self.__range__])
        self.bus.write_i2c_block_data(self.__address__, self.Register.second, [self.__gain__])
        self.bus.write_i2c_block_data(self.__address__, self.Register.first, [self.__scantype__])

    def onScan(self):
        try:
            if self.bus.read_i2c_block_data(self.__address__, self.Register.first) == 0xff:
                return True
        except IOError:
            # hack : der sensor gibt keine gueltigen i2c bloecke aus
            return True

        return False

    def getResult(self):

        low = self.bus.read_byte_data(self.__address__, self.Register.fourth)
        high = self.bus.read_byte_data(self.__address__, self.Register.third)
        return low | (high << 8)

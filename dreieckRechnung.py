#!/usr/bin/python2.7

__author__ = 'Johannes'
import time
import math

from SRF10 import usensor
from Servo import servo
from circularbuffer import CircularBuffer


class mechaniccontrol():
    def __init__(self, mech):
        self.__mechanic__ = mech
        self.__lastAngle__ = 90
        self.__mechanic__.setAngle(self.__lastAngle__)

    def setAngle(self, grad):
        if grad != self.__lastAngle__:
            self.__mechanic__.setAngle(grad)
            self.__lastAngle__ = grad


if __name__ == '__main__':
    sensor1 = usensor.SRF10(0x70)
    sensor2 = usensor.SRF10(0x71)
    mechanic = mechaniccontrol(servo.mechanic(18))
    sensor1.setGain(8)
    sensor1.setRange(24)

    sensor2.setGain(8)
    sensor2.setRange(24)

    leftbuffer = CircularBuffer(30)     # E0
    rightbuffer = CircularBuffer(30)    # E2

    abstand = 35    # abstand der sensoren zueinander

    for i in range(30):
        leftbuffer.append(0)
        rightbuffer.append(0)

    while True:
        sensor1.startScan()

        while sensor1.onScan():
            pass
        sensor1erg = sensor1.getResult()

        if sensor1erg != 0:
            leftbuffer.popleft()
            leftbuffer.append(sensor1erg)

        time.sleep(0.01)
        sensor2.startScan()

        while sensor2.onScan():
            pass

        sensor2erg = sensor2.getResult()

        if sensor2erg != 0:
            rightbuffer.popleft()
            rightbuffer.append(sensor2erg)

        time.sleep(0.01)
        #Centercheck

        winkel = 90
        print "Left.av:", leftbuffer.median, " right.av:", rightbuffer.median

        if leftbuffer.median < rightbuffer.median:
            test = 2 * leftbuffer.median * abstand * ((math.pow(leftbuffer.median, 2) + math.pow(abstand,2) - math.pow(rightbuffer.median, 2)) / (2 * leftbuffer.median * abstand))
            print "testL: ", test
            test2 = math.pow(leftbuffer.median, 2) + math.pow(abstand, 2)
            print "test2L: ", test2
            x = math.sqrt(test2 - test)
            print "x: ", x
            winkel1 = math.pow(x, 2) * math.pow((abstand / 2), 2) - math.pow(leftbuffer.median, 2)
            winkel2 = 2 * x * (abstand / 2)
            print "winkel1L: ", winkel1
            print "winkel2L: ", winkel2
            print "WINKELl: ", winkel
            winkel = math.degrees(math.acos(winkel1 / winkel2))
            print "winkelL: ", winkel

            mechanic.setAngle(winkel)
            print "Left.av:", leftbuffer.median, " right.av:", rightbuffer.median, "links winkel:", winkel

        elif leftbuffer.median > rightbuffer.maxlen:
            #x = math.sqrt((math.pow(rightbuffer.median, 2) + math.pow(abstand, 2) - 2 * rightbuffer.median * abstand * (
            #    math.pow(rightbuffer.median, 2) + math.pow(abstand, 2)) - math.pow(leftbuffer.median, 2)) / (2 * rightbuffer.median * abstand))
            #winkel = 90 + math.degrees(math.acos((math.pow(x, 2) * math.pow((abstand / 2), 2) - math.pow(rightbuffer.median, 2)) / (2 * x * (abstand / 2))))
            test = 2 * rightbuffer.median * abstand * ((math.pow(rightbuffer.median, 2) + math.pow(abstand,2) - math.pow(leftbuffer.median, 2)) / (2 * rightbuffer.median * abstand))
            print "testR: ", test
            test2 = math.pow(rightbuffer.median, 2) + math.pow(abstand, 2)
            print "test2R: ", test2
            x = math.sqrt(test2 - test)
            print "x: ", x
            winkel1 = math.pow(x, 2) * math.pow((abstand / 2), 2) - math.pow(rightbuffer.median, 2)
            winkel2 = 2 * x * (abstand / 2)
            print "winkel1R: ", winkel1
            print "winkel2R: ", winkel2
            print "WINKELr: ", winkel
            winkel = math.degrees(math.acos(winkel1 / winkel2))
            print "winkelR: ", winkel


            mechanic.setAngle(winkel)
            print "Left.av:", leftbuffer.median, " right.av:", rightbuffer.median, "rechts winkel:", winkel

        elif leftbuffer.median == rightbuffer.median:
            mechanic.setAngle(90)
            print "Left.av:", leftbuffer.median, " right.av:", rightbuffer.median, "Mitte winkel:", winkel
    try:
        print "winkelchen ", winkel
    except Exception as e:
        print "Ahhh"
        print e.message




